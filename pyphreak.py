#!/usr/bin/env python3
#
# pyphreak: bluebox for ProjectMF
# code in public domain, enjoy phreaking!
#
import wx
import simpleaudio

class pyphreak(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)

        button1 = wx.Button(self, label='1')
        button1.Bind(wx.EVT_BUTTON, self.b1_press)
        button2 = wx.Button(self, label='2')
        button2.Bind(wx.EVT_BUTTON, self.b2_press)
        button3 = wx.Button(self, label='3')
        button3.Bind(wx.EVT_BUTTON, self.b3_press)
        button4 = wx.Button(self, label='4')
        button4.Bind(wx.EVT_BUTTON, self.b4_press)
        button5 = wx.Button(self, label='5')
        button5.Bind(wx.EVT_BUTTON, self.b5_press)
        button6 = wx.Button(self, label='6')
        button6.Bind(wx.EVT_BUTTON, self.b6_press)
        button7 = wx.Button(self, label='7')
        button7.Bind(wx.EVT_BUTTON, self.b7_press)
        button8 = wx.Button(self, label='8')
        button8.Bind(wx.EVT_BUTTON, self.b8_press)
        button9 = wx.Button(self, label='9')
        button9.Bind(wx.EVT_BUTTON, self.b9_press)
        buttonKP = wx.Button(self, label='KP')
        buttonKP.Bind(wx.EVT_BUTTON, self.bKP_press)
        button0 = wx.Button(self, label='0')
        button0.Bind(wx.EVT_BUTTON, self.b0_press)
        buttonST = wx.Button(self, label='ST')
        buttonST.Bind(wx.EVT_BUTTON, self.bST_press)
        button2600 = wx.Button(self, label='2600Hz')
        button2600.Bind(wx.EVT_BUTTON, self.b2600_press)
       
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        top_button = wx.BoxSizer(wx.VERTICAL)
        top_button.Add(button2600, 0, wx.ALL|wx.EXPAND, 4)
        main_sizer.Add(top_button, 0, wx.ALL|wx.EXPAND, 4)
        
        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        hbox1.Add(button1, 1, wx.ALL, 4)
        hbox1.Add(button2, 1, wx.ALL, 4)
        hbox1.Add(button3, 1, wx.ALL, 4)
        main_sizer.Add(hbox1, 0, wx.ALL, 4)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        hbox2.Add(button4, 1, wx.ALL, 4)
        hbox2.Add(button5, 1, wx.ALL, 4)
        hbox2.Add(button6, 1, wx.ALL, 4)
        main_sizer.Add(hbox2, 0, wx.ALL, 4)
        
        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        hbox3.Add(button7, 1, wx.ALL, 4)
        hbox3.Add(button8, 1, wx.ALL, 4)
        hbox3.Add(button9, 1, wx.ALL, 4)
        main_sizer.Add(hbox3, 0, wx.ALL, 4)
        
        hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        hbox4.Add(buttonKP, 1, wx.ALL, 4)
        hbox4.Add(button0, 1, wx.ALL, 4)
        hbox4.Add(buttonST, 1, wx.ALL, 4)
        main_sizer.Add(hbox4, 0, wx.ALL, 4)
        
        # Key bindings
        b1 = wx.NewIdRef(count=1)
        b2 = wx.NewIdRef(count=1)
        b3 = wx.NewIdRef(count=1)
        b4 = wx.NewIdRef(count=1)
        b4 = wx.NewIdRef(count=1)
        b5 = wx.NewIdRef(count=1)
        b6 = wx.NewIdRef(count=1)
        b7 = wx.NewIdRef(count=1)
        b8 = wx.NewIdRef(count=1)
        b9 = wx.NewIdRef(count=1)
        b0 = wx.NewIdRef(count=1)
        bKP = wx.NewIdRef(count=1)
        bST = wx.NewIdRef(count=1)
        b2600 = wx.NewIdRef(count=1)

        self.Bind(wx.EVT_MENU, self.b1_press, id=b1)
        self.Bind(wx.EVT_MENU, self.b2_press, id=b2)
        self.Bind(wx.EVT_MENU, self.b3_press, id=b3)
        self.Bind(wx.EVT_MENU, self.b4_press, id=b4)
        self.Bind(wx.EVT_MENU, self.b5_press, id=b5)
        self.Bind(wx.EVT_MENU, self.b6_press, id=b6)
        self.Bind(wx.EVT_MENU, self.b7_press, id=b7)
        self.Bind(wx.EVT_MENU, self.b8_press, id=b8)
        self.Bind(wx.EVT_MENU, self.b9_press, id=b9)
        self.Bind(wx.EVT_MENU, self.b0_press, id=b0)
        self.Bind(wx.EVT_MENU, self.bKP_press, id=bKP)
        self.Bind(wx.EVT_MENU, self.bST_press, id=bST)
        self.Bind(wx.EVT_MENU, self.b2600_press, id=b2600)

        # Keyboard shortcuts
        self.accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL, ord('1'), b1),
                                              (wx.ACCEL_CTRL, ord('2'), b2),
                                              (wx.ACCEL_CTRL, ord('3'), b3),
                                              (wx.ACCEL_CTRL, ord('4'), b4),
                                              (wx.ACCEL_CTRL, ord('5'), b5),
                                              (wx.ACCEL_CTRL, ord('6'), b6),
                                              (wx.ACCEL_CTRL, ord('7'), b7),
                                              (wx.ACCEL_CTRL, ord('8'), b8),
                                              (wx.ACCEL_CTRL, ord('9'), b9),
                                              (wx.ACCEL_CTRL, ord('0'), b0),
                                              (wx.ACCEL_CTRL, ord('K'), bKP),
                                              (wx.ACCEL_CTRL, ord('S'), bST),
                                              (wx.ACCEL_CTRL, ord('H'), b2600),
                                             ])
        self.SetAcceleratorTable(self.accel_tbl)
        
        self.SetAutoLayout(True)
        self.SetSizer(main_sizer)
        self.Layout()

    # Audio triggers
    def b1_press(self, event):
        print('1')
        snd_1_f = 'tones/1.wav'
        snd_1 = simpleaudio.WaveObject.from_wave_file(snd_1_f)
        play_tone = snd_1.play()
        play_tone.wait_done()
    
    def b2_press(self, event):
        print('2')
        snd_2_f = 'tones/2.wav'
        snd_2 = simpleaudio.WaveObject.from_wave_file(snd_2_f)
        play_tone = snd_2.play()
        play_tone.wait_done()
    
    def b3_press(self, event):
        print('3')
        snd_3_f = 'tones/3.wav'
        snd_3 = simpleaudio.WaveObject.from_wave_file(snd_3_f)
        play_tone = snd_3.play()
        play_tone.wait_done()
    
    def b4_press(self, event):
        print('4')
        snd_4_f = 'tones/4.wav'
        snd_4 = simpleaudio.WaveObject.from_wave_file(snd_4_f)
        play_tone = snd_4.play()
        play_tone.wait_done()
    
    def b5_press(self, event):
        print('5')
        snd_5_f = 'tones/5.wav'
        snd_5 = simpleaudio.WaveObject.from_wave_file(snd_5_f)
        play_tone = snd_5.play()
        play_tone.wait_done()

    def b6_press(self, event):
        print('6')
        snd_6_f = 'tones/6.wav'
        snd_6 = simpleaudio.WaveObject.from_wave_file(snd_6_f)
        play_tone = snd_6.play()
        play_tone.wait_done()
    
    def b7_press(self, event):
        print('7')
        snd_7_f = 'tones/7.wav'
        snd_7 = simpleaudio.WaveObject.from_wave_file(snd_7_f)
        play_tone = snd_7.play()
        play_tone.wait_done()
    
    def b8_press(self, event):
        print('8')
        snd_8_f = 'tones/8.wav'
        snd_8 = simpleaudio.WaveObject.from_wave_file(snd_8_f)
        play_tone = snd_8.play()
        play_tone.wait_done()

    def b9_press(self, event):
        print('9')
        snd_9_f = 'tones/9.wav'
        snd_9 = simpleaudio.WaveObject.from_wave_file(snd_9_f)
        play_tone = snd_9.play()
        play_tone.wait_done()
    
    def bKP_press(self, event):
        print('KP')
        snd_KP_f = 'tones/KP.wav'
        snd_KP = simpleaudio.WaveObject.from_wave_file(snd_KP_f)
        play_tone = snd_KP.play()
        play_tone.wait_done()
    
    def b0_press(self, event):
        print('0')
        snd_0_f = 'tones/0.wav'
        snd_0 = simpleaudio.WaveObject.from_wave_file(snd_0_f)
        play_tone = snd_0.play()
        play_tone.wait_done()
    
    def bST_press(self, event):
        print('ST')
        snd_ST_f = 'tones/ST.wav'
        snd_ST = simpleaudio.WaveObject.from_wave_file(snd_ST_f)
        play_tone = snd_ST.play()
        play_tone.wait_done()
        
    def b2600_press(self, event):
        print('2600Hz')
        snd_2600_f = 'tones/2600.wav'
        snd_2600 = simpleaudio.WaveObject.from_wave_file(snd_2600_f)
        play_tone = snd_2600.play()
        play_tone.wait_done()

# Declare application frame
class pyphreak_frame(wx.Frame):
    def __init__(self):
        super().__init__(None, title='pyphreak: bluebox', size=(280,280))
        self.SetBackgroundColour((100, 149, 237, 255))
        panel = pyphreak(self)
        self.Show()

# Main: phreakery
if __name__ == '__main__':
    app = wx.App(redirect=False)
    frame = pyphreak_frame()
    app.MainLoop()
